package Tema08.Ejercicio07;
import Tema08.Ejercicio06.Bicicleta;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;
public class Ejercicio07 {
    private Paciente[] pacientes = new Paciente[500]; //Arrays
    private Atender[] atendidos = new Atender[40];
    private Alta[] altas = new Alta[500];
    private Guardar[] guardardador = new Guardar[500];
    private int numPacienteAlta = 0; // Contador de pacientes en alta.
    public Ejercicio07() {
        Scanner lector = new Scanner(System.in);
        boolean salir = true;
        do {
            System.out.println("*************************");
            System.out.println("******  URGENCIAS  ******");
            System.out.println("*************************");
            System.out.println("1. Nuevo paciente ...");
            System.out.println("2. Atender paciente ...");
            System.out.println("3. Consultas ...");
            System.out.println("4. Alta médica ...");
            System.out.println("-------------------------");
            System.out.println("0. Salir");
            int opcion = lector.nextInt();
            lector.nextLine();
            switch (opcion) {
                case 0:
                    salir = false;
                    break;
                case 1:
                    nuevoPaciente();
                    break;
                case 2:
                    atenderPaciente();
                    break;
                case 3:
                    consultas();
                    break;
                case 4:
                    altaMedica();
                    break;
            }
        } while (salir);
    }
    public void nuevoPaciente() {
        Calendar fecha = new GregorianCalendar();
        Scanner lector = new Scanner(System.in);
        boolean bolSip = true;
        int sip; // Se verifica que la persona con el SIP que pongamos no exista
        do {
            System.out.println("SIP: ");
            sip = lector.nextInt();
            lector.nextLine();
            bolSip = false;
            for (int i = 0; i < pacientes.length; i++) { //Recorre todo el array, si la posicion no es null
                if (pacientes[i] != null && pacientes[i].getSip() == sip) {
                    bolSip = true;
                    System.out.println("El SIP ya existe vuleve a introducirlo");
                    System.out.println("Pulsa INTRO para continuar ...");
                    lector.nextLine();
                }
            }
        } while (bolSip);
        System.out.println("Nombre: ");
        String nombre = lector.nextLine();
        System.out.println("SEXO (V/M): ");
        char sexo = lector.next().charAt(0);
        System.out.println("EDAD: ");
        int edad = lector.nextInt();
        lector.nextLine();
        int anyoEn = fecha.get(Calendar.YEAR);
        int mesEn = fecha.get(Calendar.MONTH); //Month no me funciona no se porque
        int diaEn = fecha.get(Calendar.DAY_OF_MONTH);
        int horaEn = fecha.get(Calendar.HOUR_OF_DAY);
        int minutoEn = fecha.get(Calendar.MINUTE);
        System.out.println("Sintomatologia: ");
        String sintoma = lector.nextLine();

        int i = 0;
        boolean encontrado = false;
        while(i < pacientes.length && !encontrado) {
            if(pacientes[i] == null) {
                encontrado = true;
            } else {
                i++;
            }
        }
        if (encontrado) { // Se meten todos los datos en el array de objetos
            pacientes[i] = new Paciente(sip,nombre,sexo,edad,anyoEn,mesEn,diaEn,horaEn,minutoEn,sintoma);
            System.out.println("Añadido correctamente");
        } else {
            System.out.println("Error al anadir paciente");
        }
        System.out.println("Pulsa INTRO para continuar ...");
        lector.nextLine();
    }
    public void atenderPaciente() {
        Scanner lector = new Scanner(System.in);
        int siEsta = 0;
        int numPacienteAtender = 0;
        System.out.println("Introduzca el numero SIP: ");
        int sipAtender = lector.nextInt();
        lector.nextLine();
        for (int i = 0; i < pacientes.length; i++) { // Se verifica que el SIP sea igual a uno de los guardados en el
            if (pacientes[i] != null && pacientes[i].getSip() == sipAtender) { //  array pacientes
                numPacienteAtender = i;
                siEsta++; //Si ha encontrado el SIP guarda la posicion de array donde esta el paciente y activa el if de abajo
            }
        }
        if (siEsta > 0) {
            pacientes[numPacienteAtender].imprimirPaciente();
            System.out.println("TEMPERATURA: ");
            float temperaturaAtender = lector.nextFloat();
            lector.nextLine();
            System.out.println("PPM: ");
            int ppmAtender = lector.nextInt();
            lector.nextLine();
            System.out.println("TEN SIS: ");
            int tenSisAtender = lector.nextInt();
            lector.nextLine();
            System.out.println("TEN DIAS");
            int tenDiasAtender = lector.nextInt();
            lector.nextLine();
            atendidos[numPacienteAtender] = new Atender(temperaturaAtender,ppmAtender,tenSisAtender,tenDiasAtender);
            siEsta = 0;
            System.out.println("Datos introducidos correctamente");
        } else if (siEsta == 0) { //Si despues del for anterior no encuentra el SIP introducido vendra aqui
            System.out.println("No existe ningun paciente con ese SIP");
        }
        System.out.println("Pulsa INTRO para continuar ...");
        lector.nextLine();
    }
    public void consultas() {
        Scanner lector = new Scanner(System.in);
        boolean salirConsultas = true;
        do {
            System.out.println("***************");
            System.out.println("** CONSULTAS **");
            System.out.println("***************");
            System.out.println("1. Por Sip ...");
            System.out.println("2. Por fechas ...");
            System.out.println("3. Estadisticas");
            System.out.println("4. Mostrar historico mensual");
            System.out.println("----------------------------");
            System.out.println("0. Volver al menu principal");
            int opcionConsultas = lector.nextInt();
            lector.nextLine();
            switch (opcionConsultas) {
                case 0:
                    salirConsultas = false;
                    break;
                case 1:
                    System.out.println("Introduce SIP a consultar: ");
                    int sipConsultar = lector.nextInt();
                    lector.nextLine();
                    for (int i = 0; i < guardardador.length; i++) {
                        if (guardardador[i] != null && guardardador[i].getSip() == sipConsultar) {
                            System.out.println(guardardador[i].imprimirGuardador());
                            System.out.println("---------------------------------");
                        }
                    }
                    System.out.println("Pulsa INTRO para continuar ...");
                    lector.nextLine();
                    break;
                case 2:
                    System.out.println("Introduce la fecha a buscar:");
                    System.out.println("DIA: ");
                    int diaCon = lector.nextInt(); //Case 2 pide el dia,mes y año, y si coincide se imprimen
                    lector.nextLine();
                    System.out.println("MES: "); // Mes sigue sin funcionar
                    int mesCon = lector.nextInt();
                    lector.nextLine();
                    System.out.println("ANYO: ");
                    int anyoCon = lector.nextInt();
                    lector.nextLine();
                    for (int i = 0; i < guardardador.length; i++) {
                        if (guardardador[i] != null && guardardador[i].getDiaEnt() == diaCon && guardardador[i].getMesEnt() == mesCon && guardardador[i].getAnyoEnt() == anyoCon) {
                            System.out.println(guardardador[i].imprimirGuardador());
                            System.out.println("----------------------------------");
                        }
                    }
                    System.out.println("Pulsa INTRO para continuar ...");
                    lector.nextLine();
                    break;
                case 3:
                    float mediaTemperatura = 0;
                    int mediaPpm = 0;
                    int mediaTenSis = 0;
                    int mediaTenDias = 0;
                    int mediaEdad = 0;
                    int porcentajeHombres = 0;
                    int porcentajeMujeres = 0;
                    int contadorEstadisticas = 0;

                    for (int i = 0; i < guardardador.length; i++) {
                        if (guardardador[i] != null) { //Consultas se sacan la media y ...
                            mediaTemperatura=mediaTemperatura + guardardador[i].getTemperatura();
                            mediaPpm = mediaPpm + guardardador[i].getPpm();
                            mediaTenSis = mediaTenSis + guardardador[i].getTenSis();
                            mediaTenDias = mediaTenDias + guardardador[i].getTenDias();
                            mediaEdad = mediaEdad + guardardador[i].getEdad();
                            if (guardardador[i].getSexo() == 'v' || guardardador[i].getSexo() == 'V') {
                                porcentajeHombres = porcentajeHombres + guardardador[i].getSexo();
                            } else if (guardardador[i].getSexo() == 'm' || guardardador[i].getSexo() == 'M') {
                                porcentajeMujeres = porcentajeMujeres + guardardador[i].getSexo();
                            }
                            contadorEstadisticas++;
                        }
                    }
                    mediaTemperatura = mediaTemperatura/contadorEstadisticas;
                    mediaPpm = mediaPpm / contadorEstadisticas;
                    mediaTenSis = mediaTenSis + mediaTenDias/(contadorEstadisticas*2);
                    mediaEdad = mediaEdad/contadorEstadisticas;
                    contadorEstadisticas = porcentajeHombres + porcentajeMujeres;
                    porcentajeHombres = porcentajeHombres * 100 / contadorEstadisticas;
                    porcentajeMujeres = porcentajeMujeres * 100 / contadorEstadisticas;

                    System.out.println("Media temperatura: " + mediaTemperatura);
                    System.out.println("Media tension arterial: " + mediaTenSis);
                    System.out.println("Media de edad: " + mediaEdad);
                    System.out.println("Porcentaje hombres: " + porcentajeHombres + " %");
                    System.out.println("Porcentaje mujeres: " + porcentajeMujeres + " %");
                    System.out.println("--------------------------------");
                    System.out.println("Pulsa INTRO para continuar ...");
                    lector.nextLine();
                    break;
                case 4:
                    //Imprime el array de objetos guardador en vertical sin tabla que es mas facil.
                    for (int i = 0; i < guardardador.length; i++) {
                        if (guardardador[i] != null) {
                            System.out.println(guardardador[i].imprimirGuardador());
                            System.out.println("-------------------------------");
                        }
                    }
                    System.out.println("Pulsa INTRO para continuar ...");
                    lector.nextLine();
                    break;
            }
        } while (salirConsultas);
    }
    public void altaMedica() {
        Scanner lector = new Scanner(System.in);
        Calendar fecha = new GregorianCalendar();
        System.out.println("Introduce el SIP: ");
        int sipBorrar = lector.nextInt();
        lector.nextLine();
        for (int i = 0; i < pacientes.length; i++) { //La misma forma de buscar el SIP con el SIP introducido
            if (pacientes[i] != null && pacientes[i].getSip() == sipBorrar) {
                System.out.println("Indica el motivo del alta:"); //Se recojen los ultimos datos del alta
                String motivo = lector.nextLine();
                int anyoAlta = fecha.get(Calendar.YEAR);
                int mesAlta = fecha.get(Calendar.MONTH); //Este month tampoco funciona
                int diaAlta = fecha.get(Calendar.DAY_OF_MONTH);
                int horaAlta = fecha.get(Calendar.HOUR_OF_DAY);
                int minutoAlta = fecha.get(Calendar.MINUTE);
                altas[i] = new Alta(motivo,horaAlta,minutoAlta,diaAlta,mesAlta,anyoAlta);
                guardardador[numPacienteAlta] = new Guardar(pacientes[i].getSip(),pacientes[i].getNombre(),pacientes[i].getSexo(),pacientes[i].getDia(),pacientes[i].getMes(),pacientes[i].getAnyo(),pacientes[i].getHora(),pacientes[i].getMinuto(),pacientes[i].getEdad(),pacientes[i].getSintomatologia(),atendidos[i].getTemperatura(),atendidos[i].getPpm(),atendidos[i].getTenSis(),atendidos[i].getTenDias(),altas[i].getDia(),altas[i].getMes(),altas[i].getAnyo(),altas[i].getHoraAlta(),altas[i].getMinutoAlta(),altas[i].getMotivo());
                atendidos[i] = null; //Todos los datos de los arrays de objetos pacientes, atender y alta, pasan a guardarse
                altas[i] = null; // en otro array (guardador) con mas espacio y ordenados, para dejar que nuevos pacientes puedan
                //   ser atendidos y mas altas.
            }
        }
        numPacienteAlta++; // Esto casi que no hace falta ya que cuando se guarda un valor en altas se borra al instante
    }
}
