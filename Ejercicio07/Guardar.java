package Tema08.Ejercicio07;

public class Guardar {
    private int sip;
    private String nombre;
    private char sexo;
    private int diaEnt;
    private int mesEnt;
    private int anyoEnt;
    private int horaEnt;
    private int minutoEnt;
    private int edad;
    private String sintomatologia;
    private float temperatura;
    private int ppm;
    private int tenSis;
    private int tenDias;
    private int diaAlta;
    private int mesAlta;
    private int anyoAlta;
    private int horasAlta;
    private int minutoAlta;
    private String motivo;

    public Guardar(int sip, String nombre, char sexo, int diaEnt, int mesEnt, int anyoEnt, int horaEnt, int minutoEnt, int edad, String sintomatologia, float temperatura, int ppm, int tenSis, int tenDias, int diaAlta, int mesAlta, int anyoAlta, int horasAlta, int minutoAlta, String motivo) {
        this.sip = sip;
        this.nombre = nombre;
        this.sexo = sexo;
        this.diaEnt = diaEnt;
        this.mesEnt = mesEnt;
        this.anyoEnt = anyoEnt;
        this.horaEnt = horaEnt;
        this.minutoEnt = minutoEnt;
        this.edad = edad;
        this.sintomatologia = sintomatologia;
        this.temperatura = temperatura;
        this.ppm = ppm;
        this.tenSis = tenSis;
        this.tenDias = tenDias;
        this.diaAlta = diaAlta;
        this.mesAlta = mesAlta;
        this.anyoAlta = anyoAlta;
        this.horasAlta = horasAlta;
        this.minutoAlta = minutoAlta;
        this.motivo = motivo;
    }

    public int getSip() {
        return sip;
    }

    public void setSip(int sip) {
        this.sip = sip;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public int getDiaEnt() {
        return diaEnt;
    }

    public void setDiaEnt(int diaEnt) {
        this.diaEnt = diaEnt;
    }

    public int getMesEnt() {
        return mesEnt;
    }

    public void setMesEnt(int mesEnt) {
        this.mesEnt = mesEnt;
    }

    public int getAnyoEnt() {
        return anyoEnt;
    }

    public void setAnyoEnt(int anyoEnt) {
        this.anyoEnt = anyoEnt;
    }

    public int getHoraEnt() {
        return horaEnt;
    }

    public void setHoraEnt(int horaEnt) {
        this.horaEnt = horaEnt;
    }

    public int getMinutoEnt() {
        return minutoEnt;
    }

    public void setMinutoEnt(int minutoEnt) {
        this.minutoEnt = minutoEnt;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getSintomatologia() {
        return sintomatologia;
    }

    public void setSintomatologia(String sintomatologia) {
        this.sintomatologia = sintomatologia;
    }

    public float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(float temperatura) {
        this.temperatura = temperatura;
    }

    public int getPpm() {
        return ppm;
    }

    public void setPpm(int ppm) {
        this.ppm = ppm;
    }

    public int getTenSis() {
        return tenSis;
    }

    public void setTenSis(int tenSis) {
        this.tenSis = tenSis;
    }

    public int getTenDias() {
        return tenDias;
    }

    public void setTenDias(int tenDias) {
        this.tenDias = tenDias;
    }

    public int getDiaAlta() {
        return diaAlta;
    }

    public void setDiaAlta(int diaAlta) {
        this.diaAlta = diaAlta;
    }

    public int getMesAlta() {
        return mesAlta;
    }

    public void setMesAlta(int mesAlta) {
        this.mesAlta = mesAlta;
    }

    public int getAnyoAlta() {
        return anyoAlta;
    }

    public void setAnyoAlta(int anyoAlta) {
        this.anyoAlta = anyoAlta;
    }

    public int getHorasAlta() {
        return horasAlta;
    }

    public void setHorasAlta(int horasAlta) {
        this.horasAlta = horasAlta;
    }

    public int getMinutoAlta() {
        return minutoAlta;
    }

    public void setMinutoAlta(int minutoAlta) {
        this.minutoAlta = minutoAlta;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }
    public String imprimirGuardador() {

        return String.format("SIP: %d%n",sip) +
                String.format("NOMBRE: %s%n",nombre) +
                String.format("Sexo: %c%n",sexo) +
                String.format("Fecha entrada: %d-%d-%d%n",diaEnt,mesEnt,anyoEnt) +
                String.format("Hora Entrada: %d:%d%n",horaEnt,minutoEnt) +
                String.format("Edad: %d años%n",edad) +
                String.format("Sintoma: %s%n",sintomatologia) +
                String.format("Temp: %.1f grados%n",temperatura) +
                String.format("PPM: %d%n",ppm) +
                String.format("TenSis: %d%n",tenSis) +
                String.format("TenDias: %d%n",tenDias) +
                String.format("Fecha de alta: %d-%d-%d%n",diaAlta,mesAlta,anyoAlta) +
                String.format("Hora de alta: %d:%d%n",horasAlta,minutoAlta) +
                String.format("Motivo alta: %s%n",motivo);
    }
}
