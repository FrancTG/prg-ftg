package Tema08.Ejercicio07;


import java.util.GregorianCalendar;

public class Paciente {
    private int sip;
    private String nombre;
    private char sexo;
    private int edad;
    private int anyo;
    private int mes;
    private int dia;
    private int hora;
    private int minuto;
    private String sintomatologia;

    public Paciente(int sip, String nombre,char sexo,int edad,int anyo,int mes,int dia,int hora,int minuto,String sintomatologia) {
        this.sip = sip;
        this.nombre = nombre;
        this.sexo = sexo;
        this.edad = edad;
        this.anyo = anyo;
        this.mes = mes;
        this.dia = dia;
        this.hora = hora;
        this.minuto = minuto;
        this.sintomatologia = sintomatologia;
    }

    public int getSip() {
        return sip;
    }

    public void setSip(int sip) {
        this.sip = sip;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getAnyo() {
        return anyo;
    }

    public void setAnyo(int anyo) {
        this.anyo = anyo;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinuto() {
        return minuto;
    }

    public void setMinuto(int minuto) {
        this.minuto = minuto;
    }

    public String getSintomatologia() {
        return sintomatologia;
    }

    public void setSintomatologia(String sintomatologia) {
        this.sintomatologia = sintomatologia;
    }

    public String imprimirPaciente() {
        return  String.format("SIP: %d%n",sip) +
                String.format("Nombre: %s%n",nombre) +
                String.format("Sexo: %c%n",sexo) +
                String.format("Fecha entrada: %d-%d-%d%n",dia,mes,anyo) +
                String.format("Hora de Entrada: %d:%d%n",hora,minuto) +
                String.format("Sintoma: %s",sintomatologia);
    }
}
