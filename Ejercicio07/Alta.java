package Tema08.Ejercicio07;

public class Alta {
    private String motivo;
    private int dia;
    private int mes;
    private int anyo;
    private int horaAlta;
    private int minutoAlta;

    public Alta(String motivo,int horaAlta,int minutoAlta,int dia,int mes,int anyo) {
        this.motivo = motivo;
        this.horaAlta = horaAlta;
        this.minutoAlta = minutoAlta;
        this.dia = dia;
        this.mes = mes;
        this.anyo = anyo;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAnyo() {
        return anyo;
    }

    public void setAnyo(int anyo) {
        this.anyo = anyo;
    }

    public int getHoraAlta() {
        return horaAlta;
    }

    public void setHoraAlta(int horaAlta) {
        this.horaAlta = horaAlta;
    }

    public int getMinutoAlta() {
        return minutoAlta;
    }

    public void setMinutoAlta(int minutoAlta) {
        this.minutoAlta = minutoAlta;
    }
}
